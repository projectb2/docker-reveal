
-- Description: Expand some uncommon LaTeX math commands
-- Required: pandoc v2.6

function Math(el)
    local text = el.text
    text = text:gsub(" %-> ", " \\rightarrow ")
    text = text:gsub(" <%- ", " \\leftarrow ")
    text = text:gsub(" <=> ", " \\leftrightharpoons ")
    text = text:gsub(" %. ", " \\, ")
    text = text:gsub("([^0-9. ])%.([^0-9. ])", "%1\\,%2")
    return pandoc.Math(el.mathtype, text)
end

