
-- Description: Transclude content of linked Markdown files
-- Usage: Optional link attribute base-header-level specifies base header level for file transcluded
-- Required: pandoc v2.6

-- Note: Only links occurring by itself in a paragraph will be processed
-- Note: Transclusion is non-recursive

local function file_exists(name)
    local f = io.open(name, "r")
    if f ~= nil then
        io.close(f)
        return true
    else
        return false
    end
end

function Para(el)
    if #el.content == 1 and el.content[1].t == "Link" then
        local link  = el.content[1]
        local file  = link.target
        local level = tonumber(link.attributes["base-header-level"]) or 1
        if string.match(file, "%.md$") then
            if file_exists(file) then
                io.input(file)
                doc = pandoc.read(io.read("all"))
                for i, el in pairs(doc.blocks) do
                    if el.t == "Header" then
                        el.level = el.level + level - 1
                    end
                end
                return doc.blocks
            else
                local msg = "Cannot open %s\n"
                io.stderr:write(msg:format(file))
                return el
            end
        end
    end
end

