
-------------------------------------------------------
-- Description: 
--      - Conditional output acc. to output metadata. 
--      - Removes certain blocks
-- Required: pandoc v2.6
-- Example of usage:
--      ::: report
--      example
--      :::
-------------------------------------------------------

local output

function get_output (meta)
    output = pandoc.utils.stringify(meta.output or {})
end

function Math(el)


    if el.mathtype == pandoc.DisplayMath then
        return 'codeblock' with class math
    elseif el.mathtype == pandoc.InlineMath then
   "asdfasdf".."asfsdfaf" 

    if 
    if el.classes:math







    if output == "report" then
        if el.classes:includes("report") then
            return el.content
        elseif el.classes:includes("presentation") then
            return {}
        elseif el.classes:includes("notes") then
            return {}
        else
            return el
        end
    elseif output == "presentation" then
        if el.classes:includes("presentation") then
            return el.content
        elseif el.classes:includes("report") then
            return {}
        else
            return el
        end
    else
        return el
    end
end

return {{Meta = get_output}, {Div = replace}}

